import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { Pokemon } from 'src/app/models/pokemon.model';
import { CatchPokemonService } from 'src/app/services/catch-pokemon.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.css']
})
export class ProfilePage implements OnInit {

  get user(): User | undefined {
    return this.userService.user;
  }

  username = this.user?.username;

  get pokemon(): Pokemon[] {

    if (this.userService.user) {
      return this.userService.user.pokemon;
    }
    return [];
  }

  constructor(
    private userService: UserService,
    private readonly catchPokemonService: CatchPokemonService
  ) { }

  ngOnInit(): void {
  }

  onRemoveAllClick() {
    if (this.pokemon.length > 0 && confirm("Are you sure you want to free all your pokemon?")) {
      this.catchPokemonService.freeAllPokemon()
        .subscribe()
    } else if (this.pokemon.length == 0) {
      alert("You don't have any pokemon")
    }
  }
}
