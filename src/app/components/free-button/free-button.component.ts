import { Component, OnInit } from '@angular/core';
import { CatchPokemonService } from '../../services/catch-pokemon.service';
import { UserService } from '../../services/user.service';
import { Input } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Pokemon } from '../../models/pokemon.model';
import { User } from '../../models/user.model';
import { PokemonCatalogueService } from '../../services/pokemon-catalogue.service';

@Component({
  selector: 'app-free-button',
  templateUrl: './free-button.component.html',
  styleUrls: ['./free-button.component.css']
})
export class FreeButtonComponent implements OnInit {

  public loading: boolean = false;
  public isCatched: boolean = true;

  @Input() pokemonId: number = 0;

  constructor(
    private userService: UserService,
    private readonly catchPokemonService: CatchPokemonService,
    private readonly pokemonCatalogueService: PokemonCatalogueService
  ) { }

  ngOnInit(): void {
  }

  onFreeClick(): void {
    const catchedPokemon: Pokemon | undefined = this.pokemonCatalogueService.pokemonById(this.pokemonId);
    if (catchedPokemon === undefined) {
      return
    }
    if (confirm(`Are you sure you want to free ${catchedPokemon.name}?`)) {
      this.loading = true;
      this.catchPokemonService.freePokemon(this.pokemonId)
        .subscribe({
          next: (response: User) => {
            this.loading = false;
            this.isCatched = this.userService.inCatchedPokemon(this.pokemonId);
          },
          error: (error: HttpErrorResponse) => {
            alert("this pokemon is already freed" + error.message);
          }
        })
    }
  }

}
