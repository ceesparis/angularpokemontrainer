import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { CatchPokemonService } from 'src/app/services/catch-pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.css']
})
export class CatchButtonComponent implements OnInit {

  public loading: boolean = false;
  public isCatched: boolean = false;
  @Input() pokemonId: number = 0;


  constructor(
    private userService: UserService,
    private readonly catchPokemonService: CatchPokemonService
  ) { }

  ngOnInit(): void {
    this.isCatched = this.userService.inCatchedPokemon(this.pokemonId);
  }

  onCatchClick(): void {
    this.catchPokemonService.catchPokemon(this.pokemonId)
      .subscribe({
        next: (response: User) => {
          if (!this.isCatched) {
            this.loading = true;
            setTimeout(() => {
              this.loading = false;
            }, 1000)
          }
          this.isCatched = this.userService.inCatchedPokemon(this.pokemonId);
        },
        error: (error: HttpErrorResponse) => {
          alert("this pokemon is already catched!" + error.message);
        }
      })
  }

}
