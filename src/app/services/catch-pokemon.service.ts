import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { UserService } from './user.service';
import { finalize, Observable } from 'rxjs';
import { tap } from 'rxjs';

const { apiKey, apiTrainers } = environment

@Injectable({
  providedIn: 'root'
})
export class CatchPokemonService {


  constructor(
    private readonly pokemonService: PokemonCatalogueService,
    private readonly userService: UserService,
    private http: HttpClient,
  ) { }

  public catchPokemon(pokemonId: number): Observable<User> {

    if (!this.userService.user) {
      throw new Error("catchPokemon: there is no user")
    }

    const user: User = this.userService.user;

    const pokemon: Pokemon | undefined = this.pokemonService.pokemonById(pokemonId);

    if (!pokemon) {
      throw new Error("catchPokemon: no pokemon with id" + pokemonId);
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

    if (this.userService.inCatchedPokemon(pokemonId)) {
      alert("pokemon already catched")
      return this.http.patch<User>(`${apiTrainers}/${user.id}`, {
        pokemon: [...user.pokemon]
      }, {
        headers
      })
    }

    return this.http.patch<User>(`${apiTrainers}/${user.id}`, {
      pokemon: [...user.pokemon, pokemon]
    }, {
      headers
    })
      .pipe(
        tap((updatedUser: User) => {
          this.userService.user = updatedUser;
        }),
        finalize(() => {
        })
      )
  }

  public freePokemon(pokemonId: number): Observable<User> {

    if (!this.userService.user) {
      throw new Error("catchPokemon: there is no user")
    }

    const user: User = this.userService.user;

    const pokemon: Pokemon | undefined = this.pokemonService.pokemonById(pokemonId);

    if (!pokemon) {
      throw new Error("catchPokemon: no pokemon with id" + pokemonId);
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

    // return user with pokemon array without the freed pokemon 
    return this.http.patch<User>(`${apiTrainers}/${user.id}`, {
      pokemon: [...user.pokemon].filter((pokemon: Pokemon) => pokemon.id !== pokemonId)
    }, {
      headers
    })
      .pipe(
        tap((updatedUser: User) => {
          this.userService.user = updatedUser;
        }),
        finalize(() => {
        })
      )
  }

  public freeAllPokemon(): Observable<User> {

    if (!this.userService.user) {
      throw new Error("catchPokemon: there is no user")
    }

    const user: User = this.userService.user;


    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

    // return user with empty pokemon array 
    return this.http.patch<User>(`${apiTrainers}/${user.id}`, {
      pokemon: []
    }, {
      headers
    })
      .pipe(
        tap((updatedUser: User) => {
          this.userService.user = updatedUser;
        }),
        finalize(() => {
        })
      )
  }
}
