import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { ApiRequest } from '../models/apiRequest.model';
import { pipe, finalize } from 'rxjs';

import { StorageKeys } from 'src/enums/storage-keys.enum';
import { StorageUtil } from '../utils/storage.util';

const { apiPokemon } = environment;
const IMAGE_URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon"

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemon: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  get pokemon(): Pokemon[] {
    return this._pokemon;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  public findAllPokemon(): void {

    if (this._pokemon.length > 0 || this.loading) {
      return
    }

    this._loading = true;
    const localStoredPokemon = StorageUtil.storageRead<Pokemon[]>(StorageKeys.PokemonCatalogue);
    if (localStoredPokemon !== undefined) {
      this._pokemon = localStoredPokemon;
      this._loading = false;
      return
    } else {
      this.http.get<ApiRequest>(apiPokemon)
        .pipe(
          finalize(() => {
            this._loading = false;
          })
        )
        .subscribe({
          next: (response: ApiRequest) => {
            this._pokemon = response.results.map(pokemon => {
              return {
                ...pokemon,
                id: this.getId(pokemon.url),
                image: this.getImage(pokemon.url)
              }
            });
            StorageUtil.storageSave<Pokemon[]>(StorageKeys.PokemonCatalogue, this._pokemon)
          },
          error: (error: HttpErrorResponse) => {
            this._error = error.message;
          }
        })
    }
  }

  private getId(url: string): number {
    const id = Number(url.split('/').filter(Boolean).pop());
    return id;
  }

  private getImage(url: string): string {
    const id = url.split('/').filter(Boolean).pop();
    return `${IMAGE_URL}/${id}.png`;
  }

  public pokemonById(id: number): Pokemon | undefined {
    return this._pokemon.find((pokemon: Pokemon) => pokemon.id === id)
  }

}
