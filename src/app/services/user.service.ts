import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';
import { StorageKeys } from 'src/enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user?: User;

  get user(): User | undefined {
    return this._user;
  }

  set user(user: User | undefined) {
    StorageUtil.storageSave<User>(StorageKeys.User, user!)
  this._user = user;
  }

  constructor() { 
    this._user = StorageUtil.storageRead<User>(StorageKeys.User);
  }

  public inCatchedPokemon(pokemonId: number): boolean {
    if (this._user) {
      return Boolean(this.user?.pokemon?.find((pokemon: Pokemon) => pokemon.id === pokemonId))
    }
    return false;
  }

  // remove user from session storage
  public logOutUser(): void {
    StorageUtil.storageRemove(StorageKeys.User)
    this._user = undefined
  }

}
