import { ApiRequestPokemon } from "./apiRequestPokemon.model";

export interface ApiRequest {
    count: number;
    next: string;
    previous: string;
    results: ApiRequestPokemon[];
}