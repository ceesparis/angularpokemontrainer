export enum StorageKeys {
    User = "pokemon-trainer",
    PokemonCatalogue = "pokemon-catalogue"
}