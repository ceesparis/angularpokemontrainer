# Angular Pokemon Trainer

In this app, you can login as a pokemon trainer and catch pokemon. Catched pokemon will be displayed on your profile page. Happy hunting! 


## Usage

If you are new to the app, you can simply login with your new username and an account will be created automatically. If you already have an account you can login in a similar fashion. On the pokemon-catalogue page you can check out all the pokemon and catch them by clicking on the pokeball. On the profile page you can see the pokemon you catched and free them by clicking on the open pokeball. You can also release all your pokemon at once by clicking the 'free all pokemon'-button on the top of the page. 

## Installation

This application can be found on Heroku [here](https://dashboard.heroku.com/apps/arcane-reaches-82913). 
To run this app on your local machine, fork and clone this repository and install dependencies using npm install. Then to start and open the app, 'run ng serve -o'. 

## Contributing

I got the assignment and some usefull videos from Noroff School of Technology and Digital Media. The Pokemon are trademarks of Nitendo. 

## License

MIT © Cees Paris 